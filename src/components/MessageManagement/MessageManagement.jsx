import axios from "axios";
import dateFormat from "dateformat";
import { useEffect, useState } from "react";
import ReactPaginate from 'react-paginate';
import "@fortawesome/fontawesome-free/css/all.css";

const baseUrl = "http://localhost:8090/api/v1";

class Message {
    constructor(id, textMessage, imageUrl, createdAt, modifiedAt, scheduled, status) {
        this.id = id;
        this.textMessage = textMessage;
        this.imageUrl = imageUrl;
        this.createdAt = dateFormat(createdAt, "dd/mm/yyyy HH:MM:ss");
        this.modifiedAt = modifiedAt === null ? "-" : dateFormat(modifiedAt, "dd/mm/yyyy HH:MM:ss");
        this.scheduled = scheduled === null ? "-" : dateFormat(scheduled, "dd/mm/yyyy HH:MM:ss");
        this.status = status;
    }
}

const MessageStatus = {
    OK: "OK",
    WARNING: "Warning",
    FAILED: "Failed",
    SENDING: "Sending",
}

const MessageManagement = (props) => {
    const { editing, toggleEdit } = props;
    const [messages, setMessages] = useState([]);
    const [page, setPage] = useState(1);
    const [totalPages, setTotalPages] = useState(1);

    useEffect(() => fetchData(), []);
    useEffect(() => fetchData(), [page]);
    useEffect(() => {
        if (!editing) {
            fetchData();
        }
    }, [editing]);

    const fetchData = () => {
        axios.get(`${baseUrl}/messages?page=${page}`)
            .then(response => {
                setMessages(response.data.messages.map(msg => new Message(
                    msg.id,
                    msg.textMessage,
                    msg.imageUrl,
                    msg.createdAt,
                    msg.modifiedAt,
                    msg.scheduled,
                    msg.status
                )));
                setTotalPages(response.data.totalPages);
            })
            .catch(error => console.log('error', error));
    }

    const handlePageClick = (e) => {
        setPage(e.selected + 1);
    };

    const deleteMessage = (id) => {
        axios.delete(`${baseUrl}/messages/${id}`)
            .then(response => {
                console.log(response.data);
                alert("Xóa thành công");
                fetchData();
            })
            .catch(error => console.log('error', error));
    }

    return (
        <div>
            <table className="table table-hover">
                <thead>
                    <tr>
                        <th scope="col">STT</th>
                        <th scope="col">Message</th>
                        <th scope="col">Attachment</th>
                        <th scope="col">Recipients</th>
                        <th scope="col">Created At</th>
                        <th scope="col">Modified At</th>
                        <th scope="col">Scheduled</th>
                        <th scope="col">Status</th>
                        <th scope="col">Action</th>
                    </tr>
                </thead>
                <tbody>
                    {
                        messages.map(msg => {
                            var badge = "badge ";
                            if (msg.status === MessageStatus.OK) {
                                badge += "bg-success";
                            } else if (msg.status === MessageStatus.WARNING) {
                                badge += "bg-warning text-dark";
                            } else if (msg.status === MessageStatus.FAILED) {
                                badge += "bg-danger";
                            } else {
                                badge += "bg-primary";
                            }
                            return (
                                <tr key={msg.id}>
                                    <th scope="row">{msg.id}</th>
                                    <td>{msg.textMessage}</td>
                                    <td><a href={msg.imageUrl} target="_blank">Click here</a></td>
                                    <td>
                                        <a href={`${baseUrl}/messages/${msg.id}/message_users/excel`}
                                            download rel="noopener noreferrer">Click here</a>
                                    </td>
                                    <td>{msg.createdAt}</td>
                                    <td>{msg.modifiedAt}</td>
                                    <td>{msg.scheduled}</td>
                                    <td><span className={badge + " w-100"}>{msg.status === null ? "Un-send" : msg.status}</span></td>
                                    <td>
                                        {editing &&
                                            <button className="btn" onClick={() => toggleEdit()}>
                                                <i className="fas fa-times-circle"></i>
                                            </button>
                                        }
                                        {!editing &&
                                            <div>
                                                <button className="btn" onClick={() => toggleEdit(msg.id)}>
                                                    <i className="fas fa-pen"></i>
                                                </button>
                                                <button className="btn" onClick={() => deleteMessage(msg.id)}>
                                                    <i className="fas fa-trash"></i>
                                                </button>
                                            </div>
                                        }
                                    </td>
                                </tr>
                            );
                        })
                    }
                </tbody>
            </table>
            <ReactPaginate
                previousLabel="<<"
                nextLabel=">>"
                breakLabel="..."
                breakClassName="page-item"
                breakLinkClassName="page-link"
                pageCount={totalPages}
                onPageChange={handlePageClick}
                containerClassName="pagination justify-content-center"
                pageClassName="page-item"
                pageLinkClassName="page-link"
                previousClassName="page-item"
                previousLinkClassName="page-link"
                nextClassName="page-item"
                nextLinkClassName="page-link"
                activeClassName="active"
            />
        </div>
    );
}

export default MessageManagement;